package library.entity;

import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class AddBook implements Serializable {	

	private Connection cnt = null;
	private Statement stmt = null;
	private ResultSet resultSet = null;               
	private String URL = "jdbc:derby://localhost:1527/LIBRARYDB";
       
        	List<Book> bookList = new ArrayList<Book>(); 
        public enum Status 
        { 
            onShelf, borrowed, lost, damaged,reserved; 
        }   
        
	public AddBook() {
	}
        public List<Book> getBookList() {
            return bookList;
        }
         
        public void setBookList( List<Book> bookList) {
            this.bookList = bookList;
        }
	public boolean connection(String Query) {            
            boolean hasBook = false;
		try {
			Class.forName("org.apache.derby.jdbc.ClientDriver");
		}
		catch (ClassNotFoundException cnfe) {
			System.out.println("Books.java\n" + cnfe.toString());
		}
		catch (Exception e) {
			System.out.println("Books.java\n" + e.toString());
		}
                
		//for making the connection,creating the statement and update the table in the database.
		try {
                    
			cnt = DriverManager.getConnection(URL,"root","root");
			stmt = cnt.createStatement();
			resultSet = stmt.executeQuery(Query);
			if (resultSet.next()) {
                            do{
                                Book book = new Book();
				book.setBookID(resultSet.getString(1));
				book.setTitle(resultSet.getString(2));
				book.setAuthor(resultSet.getString(3));
				book.setIsbn(resultSet.getString(4));
				book.setStatus(resultSet.getString(5));
                                book.setMemberID(resultSet.getString(6));
                                book.setBorrowDate(resultSet.getDate(7));
                                book.setReturnDate(resultSet.getDate(8));
                                bookList.add(book);
                            }while(resultSet.next());
                                 hasBook = true;                                 
			}
			resultSet.close();
			stmt.close();
			cnt.close();
		}
		catch (SQLException SQLe) {
			System.out.println("Books.java\n" + SQLe.toString());
		}
                System.out.println(Query+" "+hasBook);
                if(hasBook)
                    return true;
                else 
                    return false;                
                
	}

	public void update(String Query) {
            System.out.println(Query);
		try {
			Class.forName("org.apache.derby.jdbc.ClientDriver");
		}
		catch (ClassNotFoundException cnfe) {
			System.out.println("Books.java\n" + cnfe.toString());
		}
		catch (Exception e) {
			System.out.println("Books.java\n" + e.toString());
		}
                
		//for making the connection,creating the statement and update the table in the database.
		try {
			cnt = DriverManager.getConnection(URL,"root","root");
			stmt = cnt.createStatement();
			stmt.executeUpdate(Query);
			stmt.close();
			cnt.close();
		}
		catch (SQLException SQLe) {
			System.out.println("Books.java\nError:" + SQLe.toString());
		}
	}
}