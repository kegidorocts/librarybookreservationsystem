package library.entity;

import java.io.Serializable;
import java.sql.*;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class AddMember implements Serializable{	

	private Connection cnt = null;
	private Statement stmt = null;
	private ResultSet resultSet = null;

        @Id
	private int memberID;
	private int ID;
	private String password;	
	private boolean isStaff;
        private String firstName,lastName;
	private String URL = "jdbc:derby://localhost:1527/LIBRARYDB";

	public AddMember() {
	}	

	public int getMemberID() {
		return memberID;
	}

	public int getID() {
		return ID;
	}

	public String getPassword() {
            return password;
	}	
         
        public void setPassword(String password) {
            this.password = password;
	}
        
        public String getFirstName() {
            return firstName;
	}	
         
        public void setFirstName(String firstName) {
            this.firstName = firstName;
	}
        
       public String getLastName() {
            return lastName;
	}	
         
        public void setLastName(String lastName) {
            this.lastName = lastName;
	}
		
	public boolean getIsStaff() {
            return isStaff;
	}

        public void setIsStaff(boolean isStaff) {
            this.isStaff = isStaff;
	}
        
	public boolean connection(String Query) {
                boolean hasMember = false;
		try {
			Class.forName("org.apache.derby.jdbc.ClientDriver");
		}
		catch (ClassNotFoundException cnfe) {
			System.out.println("Members.java\n" + cnfe.toString());
		}
		catch (Exception e) {
			System.out.println("Members.java\n" + e.toString());
		}
		//for making the connection,creating the statement and update the table in the database. 
		try {
			cnt = DriverManager.getConnection(URL,"root","root");
			stmt = cnt.createStatement();
			resultSet = stmt.executeQuery(Query);
			if (resultSet.next()) {
                            do{
				memberID = resultSet.getInt(1);
                                 isStaff = resultSet.getBoolean(2);
				password = resultSet.getString(3);
                                 firstName = resultSet.getString(4);
                                 lastName = resultSet.getString(5);
                                 System.out.println(password);
                            }while(resultSet.next());
                                 hasMember = true;
			}else
                            hasMember = false;
			resultSet.close();
			stmt.close();
			cnt.close();
		}
		catch (SQLException SQLe) {
			System.out.println("Members.java\n" + SQLe.toString());
		}                
                if(hasMember)
                    return true;
                else 
                    return false;
	}

	public void update(String Query) {
		try {
                    Class.forName("org.apache.derby.jdbc.ClientDriver");
		}
		catch (ClassNotFoundException cnfe) {
			System.out.println("Members.java\n" + cnfe.toString());
		}
		catch (Exception e) {
			System.out.println("Members.java\n" + e.toString());
		}	
                //for making the connection,creating the statement and update the table in the database. 
		try {
			cnt = DriverManager.getConnection(URL,"root","root");
			stmt = cnt.createStatement();
			stmt.executeUpdate(Query);
			stmt.close();
			cnt.close();
		}
		catch (SQLException SQLe) {
			System.out.println("Members.java\n" + SQLe.toString());
		}
	}
}