package library.entity;

import java.io.Serializable;
import java.sql.*;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Reserve implements Serializable{	
	private Connection cnt = null;
	private Statement stmt = null;
	private ResultSet resultSet = null;
        @Id
	private int bookID;
	private int memberID;
	private Date reserveDate;
	private Date returnDate;
	private String URL = "jdbc:derby://localhost:1527/LIBRARYDB";

	public Reserve() {
	}

	public int getBookID() {
            return bookID;
	}
        
        public void setBookID(int bookID) {
            this.bookID = bookID;
	}

	public int getMemberID() {
            return memberID;
	}
        
        public void setMemberID(int memberID) {
            this.memberID = memberID;
	}

	public Date getReserveDate() {
            return reserveDate;
	}
        
        public void setReserveDate(Date reserveDate) {
            this.reserveDate = reserveDate;
	}


	public Date getReturnDate() {
            return returnDate;
	}
        
         public void setReturnDate(Date returnDate) {
            this.returnDate = returnDate;
	}


	public boolean connection(String BookId, Date date) {
            boolean available = false;
		try {
			Class.forName("org.apache.derby.jdbc.ClientDriver");
		}
		catch (ClassNotFoundException cnfe) {
			System.out.println("Reserve.java\n" + cnfe.toString());
		}
		catch (Exception e) {
			System.out.println("Reserve.java\n" + e.toString());
		}
                
		// for making the connection,creating the statement and update the table in the database. 
		try {
			cnt = DriverManager.getConnection(URL,"root","root");
			stmt = cnt.createStatement();
			resultSet = stmt.executeQuery("SELECT * FROM Reserve WHERE BookId = '"+ BookId+"' AND"
                                + "ReturnDate <= '" + date +"' AND"
                                + "StartDate <= ReturnDate");
			if (resultSet.next()) {
                            do{
				bookID = resultSet.getInt(1);
				memberID = resultSet.getInt(2);
				reserveDate = resultSet.getDate(3);
				returnDate = resultSet.getDate(4);}
                            while(resultSet.next());
                                 available = true;                                 
			}
			resultSet.close();
			stmt.close();
			cnt.close();
		}
		catch (SQLException SQLe) {
			System.out.println("Reserve.java\n" + SQLe.toString());
		}
                 if(available)
                    return true;
                else 
                    return false;
	}

	public void update(String Query) {            
		try {
			Class.forName("org.apache.derby.jdbc.ClientDriver");
		}
		catch (ClassNotFoundException cnfe) {
			System.out.println("Reserve.java\n" + cnfe.toString());
		}
		catch (Exception e) {
			System.out.println("Reserve.java\n" + e.toString());
		}
		//for making the connection,creating the statement and update the table in the database.
		try {
			cnt = DriverManager.getConnection(URL,"root","root");
			stmt = cnt.createStatement();
                         stmt.executeUpdate(Query);                        
			stmt.close();
			cnt.close();
		}
		catch (SQLException SQLe) {
			System.out.println("Reserve.java\n" + SQLe.toString());
		}
	}
}