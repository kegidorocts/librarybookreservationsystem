/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package library.entity;

import java.sql.Date;

public class Book{
    private String title;
    private String author;
    private String isbn; 
    private AddBook.Status status;   
    private String memberID;
    private String bookID;
    private Date borrowDate;
    private Date returnDate;
     private String firstName,lastName;
      
        
        public String getFirstName() {
            return firstName;
	}	
         
        public void setFirstName(String firstName) {
            this.firstName = firstName;
	}
        
       public String getLastName() {
            return lastName;
	}	
         
        public void setLastName(String lastName) {
            this.lastName = lastName;
	}
        
	public String getBookID() {
            return bookID;
	}
        
        public void setBookID(String bookID) {
            this.bookID = bookID;
	}     
	
	public String getTitle() {
            return title;
	}
        
        public void setTitle(String title) {
            this.title = title;
	}        

	public String getAuthor() {
            return author;
	}

        public void setAuthor(String author) {
            this.author = author;
	}
        
	public String getIsbn() {
            return isbn;
	}
        
        public void setIsbn(String isbn) {
            this.isbn = isbn;
	}
                
        public String getMemberID() {
		return memberID;
	}
        
        public void setMemberID(String memberID) {
            this.memberID = memberID;
        }  
        
        public Date getBorrowDate() {
		return borrowDate;
	}

        public void setBorrowDate(Date borrowDate) {
            this.borrowDate = borrowDate;
        }  
        
        public Date getReturnDate() {
		return returnDate;
	}

        public void setReturnDate(Date returnDate) {
            this.returnDate = returnDate;
        }  
	
        public String getStatus() {
            if(status == AddBook.Status.borrowed)
                return "borrowed";
            else if(status == AddBook.Status.lost)
                return "lost";
            else if(status == AddBook.Status.damaged)
                return "damaged";
            else if(status == AddBook.Status.onShelf)
                return "On Shelf";  
            else
                return "reserved"; 
	}

        public void setStatus(String status) {
            if(status.equals("borrowed")){
                this.status = AddBook.Status.borrowed;
            }else if(status.equals("lost")){
                this.status = AddBook.Status.lost;
            }else if(status.equals("damaged")){
                this.status = AddBook.Status.damaged;
            }else if(status.equals("On Shelf")){
                this.status = AddBook.Status.onShelf;    
            }else if(status.equals("reserved"))
                this.status = AddBook.Status.reserved;
            else 
                this.status = null;
        }          
	
}