//Backing bean to support both xhtml files
package library.web;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import library.ejb.LibraryManager;
import library.entity.*;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;

@Named
@SessionScoped
public class LibraryBacking implements Serializable {
    private String hint;
    
    @EJB
    private LibraryManager libraryManager;
    private String bookID;
    private String memberID;
    private Date startDate;
    private Date endDate;
    private String firstName,lastName;
    private String title,author,isbn;
    private String status;   
    
    AddBook book = new AddBook();
    AddMember member = new AddMember();
    public LibraryBacking() {
        hint = null;
        startDate = new Date();        
    }

    public String getBookID() {
        return bookID;
    }

    public void setBookID(String bookID) {
        this.bookID = bookID;       
    }   
    
    public Date getEndDate() {
         return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }        

    public Date getStartDate() {
         return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }  
    
     public String getMemberID() {         
        return memberID;
    }
    
    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }
    
    public String getFirstName() {
        return firstName;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

     public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
     public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
     public String getAuthor() {
        return author;
    }
    
    public void setAuthor(String author) {
        this.author = author;
    }
    
    public String getIsbn() {
        return isbn;
    }
    
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
    
    public String getStatus() {        
        return status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }          
    
    public String getHint() {
        return hint;
    }

    public void addReserveBook() {  
        Reserve reserve = new Reserve();
        if(!book.connection("SELECT * FROM Books WHERE BookID = " + bookID)){
              hint = "No book with such BookID!";
        }else if(!member.connection("SELECT * FROM Members WHERE FirstName = '" + firstName + "' AND LastName = '" + lastName+"'")) {  
            hint = "There is no such member " + firstName+" "+ lastName;
        } else if(!reserve.connection(bookID,new java.sql.Date(endDate.getTime()))){
            hint = "Please choose an appropiate time period";
        }else{            
            if (status.equals("On Shelf")) {
                book.update("UPDATE Books SET BookStatus = 'reserved', BorrowDate = '"+new java.sql.Date(startDate.getTime())+
                        "', ReturnDate = '"+new java.sql.Date(endDate.getTime())+"', MemberID = '"+member.getMemberID()+"' WHERE BookID =" + bookID);
                reserve.update("INSERT INTO Reserve (MemberID, BookID, ReserveDate, ReturnDate) VALUES ('" +
                         member.getMemberID()+ "', '" + bookID + "','" + new java.sql.Date(startDate.getTime()) + "','" + new java.sql.Date(endDate.getTime()) + "')");
                hint = "You have sucessfully reserved a book!";
            }else
                hint = "The bookID " + bookID + " is "+ status +".  Please try another one.";
        }  
        //libraryManager.add(reserve);
        bookID = memberID = null;
        endDate = startDate = null;
    }
    
    public void submitDetails() {       
        String bookQuery = "SELECT * FROM Books WHERE BookID = " + bookID;        
        if(!book.connection(bookQuery)){
            hint = "No book with such BookID!";
        }else if(!member.connection("SELECT * FROM Members WHERE FirstName = '" + firstName + "' AND LastName = '" + lastName+"'")) {  
            hint = "There is no such member " + firstName+" "+ lastName;
        }else{           
            book.update("UPDATE Books SET firstName = '"+firstName+"', lastName='"+lastName
                    +"', status='"+status+"' WHERE BookID =" + bookID);
            //libraryManager.add(book);
            hint = "The details were submited!";
            firstName = lastName  = title = author = isbn = null;
        }       
      
    }  
    
     public void addBook()throws SQLException, IOException{       
        Boolean isSuccess = false;
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        try{            
            cnnct = DriverManager.getConnection("jdbc:derby://localhost/LIBRARYDB","root","root");
            String preQueryStatement = "INSERT INTO BOOKS (title,author,isbn,bookstatus)VALUES ('"+
                    title+"', '"+ author+"', '"+ isbn+"', 'On Shelf')";
            System.out.println(preQueryStatement);
            pStmnt = cnnct.prepareStatement(preQueryStatement);           
            int rowCount = pStmnt.executeUpdate();
            if(rowCount>=1){
                isSuccess=true;
            }
            pStmnt.close();
            cnnct.close();
        }catch(SQLException ex){
            while (ex != null){
                ex.printStackTrace();
                ex=ex.getNextException();
            }
        }            
        if(isSuccess) 
            hint = "A book successfully added!";
        else
            hint = "Book adding is unsuccessful";
    }  
    
    public List<Reserve> getReserveBooks() {
        return libraryManager.getAll();
    }
}
