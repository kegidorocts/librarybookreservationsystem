package library.web;

import java.io.IOException;
import java.io.Serializable;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import library.entity.AddMember;

@Named //Gives a CDI managed bean an EL name to be used in view technologies like JSF or JSP
@SessionScoped
public class Register implements Serializable {        
    private String password;
    private String hint;
    private String lastName,firstName;   
    AddMember member = new AddMember();

    public Register() { 
        hint = null;       
    }   
   
    public String getPassword() {
        return null;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
     public boolean notStaff() {
         if(!member.getIsStaff()){
             hint = "You are NOT a staff";
            return true;
         }
         else{
            hint = null;
            return false;
         }
    }  
    
    public String getFirstName() {
        return null;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
     public String getLastName() {
        return null;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getHint() {
        return hint;
    } 
    
    public String addRegistration(){       
        if(member.connection("SELECT * FROM Members WHERE FirstName = '" + firstName + "' AND LastName = '" + lastName+"'")) {  
            this.hint = "Someone else has used " + firstName+" "+ lastName
                    + " as user name already.  Please try another one.";
        }else{            
            member.setIsStaff(false);
            member.setPassword(password);
            member.setFirstName(firstName);
            member.setFirstName(lastName);
            member.update("INSERT INTO Members(isStaff, Password, firstName, lastName) VALUES " +
                   "(FALSE, '"+ password + "', '" + firstName  + "', '"+lastName+ "')");            
             hint = "You have successfully registered!";  
        }
        return null;
    }
    
    public String login() throws IOException{        
        if(member.connection("SELECT * FROM Members WHERE FirstName = '" + firstName + "' AND LastName = '" + lastName +
                 "' AND Password = '" + password+"'")){
             return "faces/index.jsp";
        }
        else{
            hint =  "Login Failed!";
            return null;
        }
    }
   
}