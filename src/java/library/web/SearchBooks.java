package library.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import library.entity.*;

@Named
@SessionScoped
public class SearchBooks implements Serializable{
     	List<Book> bookList = new ArrayList<Book>(); 
        private String searchType = "Book ID";
        private String keyword;
        private String hint;
        private AddBook book;
	//constructor of listSearchBooks
	public SearchBooks() {		
            hint = null;           	
        }
        
        public List<Book> getBookList() {
            return bookList;
        }
         
        public void setBookList( List<Book> bookList) {
            this.bookList = bookList;
        }        
      
        public String getSearchType() {
            return searchType;
        }

        public void setSearchType(String searchType) {
            this.searchType = searchType;
        }       
        
        public String getKeyword() {
            return keyword;
        }

        public void setKeyword(String keyword) {
            this.keyword = keyword;
        }      
        
        public String getHint() {
            return hint;
        }
        
        public void searchBook(){            
            String bookQuery;
            //for checking if there is a missing information
            if (!searchType.equals("Book ID")){
                 keyword = "'%" + keyword + "%'";
                 bookQuery = "SELECT BookID, Title, Author,ISBN,BookStatus,MemberId"
                    + ",BorrowDate, ReturnDate FROM Books" +
                    " WHERE " + searchType + " LIKE " + keyword;
            }else{
                book = new AddBook();
                bookQuery = "SELECT BookID, Title, Author,ISBN,BookStatus,MemberId"
                    + ",BorrowDate, ReturnDate FROM Books" +
                    " WHERE BookID = "+ keyword;  
            }
            if(!book.connection(bookQuery)){
                hint =  "No Any Match";
                keyword = null;
            }else{
                hint =  null;
                bookList = book.getBookList();
            }
                   
        }
        
}