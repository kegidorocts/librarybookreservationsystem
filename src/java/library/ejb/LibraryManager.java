//We need this session bean to persist the entity to enjoy automatic container-managed transactions.
//We also need it to retrieve all Customer entities stored in the database. 
package library.ejb;

import library.entity.*;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class LibraryManager {

    // Add business logic below. 
    @PersistenceContext
    private EntityManager em;
    
    public void add(Reserve reserve) {
        em.persist(reserve); //persist()将实例转换为 managed(托管)状态(对象发生改变EntityManager都知道) 
        //Used to insert a new object into the database(registers the object as new in the persistence context(transaction))
    }
    
    public void add(AddBook book) {
        em.persist(book);
    }
    
    public void add(AddMember member) {
        em.persist(member);
    }   
   
    
    public List<Reserve> getAll() {
        List<Reserve> results = new ArrayList<>();
        results = em.createNamedQuery("findAll", Reserve.class).getResultList();
        //createNamedQuery() defines static queries with name in mapping file or queries are defined in metadata(annotation) 
        
        return results;
    }
}
